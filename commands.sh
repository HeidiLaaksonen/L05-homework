#!/bin/bash

npm init -y
git init

npm i --save-dev mocha@10.2.0 chai@4.3.7 selenium-webdriver@4.8.0 geckodriver@4.3.2

npm pkg set 'scripts.test'='mocha'

mkdir test
touch test/main.spec.js
touch test/test.spec.js

echo "node_modules" > .gitignore

git remote add origin https://gitlab.com/HeidiLaaksonen/L05-homework

#npm run test
npm run test -- --timeout 15000

git add . --dry-run

git add .
git status
git commit -m "kommentti"
git push
git push -u origin master
