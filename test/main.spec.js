const expect = require('chai').expect;
const { Builder, By, Key, WebElement, Browser } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
require('geckodriver');

const BASE_URL = "https://en.m.wikipedia.org/wiki/N";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

describe('UI Tests', () => {
    /** @type {import('selenium-webdriver').ThenableWebDriver} */
    let driver = undefined;
    before(async () => {
        const options = new firefox.Options(); // Firefox asetukset
        options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe"); // Polku firefox.exe
        // webdriver creation
        driver = await new Builder()
            .forBrowser(Browser.FIREFOX)
            .setFirefoxOptions(options) // Asetusten asettaminen
            .build();
        await driver.get(BASE_URL);
        await sleep(2);
    });
    it('Can it find the search bar go to the search site and write on the searchbar', async () => {
        const searchbar = await driver.findElement(By.id("searchInput"));
        let enter = await searchbar.getAttribute('value');
        // Mene search sivulle
        const keystroke_sequence = [];
        keystroke_sequence.push(Key.ENTER); // Käyttäjä painaa enter
        await searchbar.sendKeys(...keystroke_sequence);
        enter = await searchbar.getAttribute('value');
        // kirjoita searchbar:iin jotain
        const input = await driver.findElement(By.className("search mf-icon-search"));
        let inputs = await input.getAttribute('value');
        const keystroke_sequences = [];
        keystroke_sequences.push("kokeilu"); // Käyttäjä kirjoittaa arvon
        await input.sendKeys(...keystroke_sequences);
        inputs = await input.getAttribute('value'); 
        await sleep(4);
    });
    after(async() => {
        await driver.close();
    });
});
